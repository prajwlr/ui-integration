import { Component, OnInit } from '@angular/core';
import { Stocks } from '../shared/stocks';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-stock-status',
  templateUrl: './stock-status.component.html',
  styleUrls: ['./stock-status.component.css']
})
export class StockStatusComponent implements OnInit {

  id: any;

  Stocks: any = [];
  constructor(public restApi: RestApiService,
    private route: ActivatedRoute) { }


    
  ngOnInit(): void {
    this.id = this.route.snapshot.params["id"];

    this.restApi.getStocksById(this.id).subscribe((data: {}) => {
      this.Stocks = data;
  });

}
}