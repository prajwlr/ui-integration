import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockOrderComponent} from './stock-order/stock-order.component';
import { StockGetComponent} from './stock-get/stock-get.component';
import { StockStatusComponent} from './stock-status/stock-status.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'stock-get' },
  { path: 'stock-order', component: StockOrderComponent },
  { path: 'stock-get', component: StockGetComponent },
  { path: 'stock-status/:id', component: StockStatusComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
