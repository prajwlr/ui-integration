import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-stock-order',
  templateUrl: './stock-order.component.html',
  styleUrls: ['./stock-order.component.css']
})
export class StockOrderComponent implements OnInit {

  @Input() stockdetails = { id:0, datetime:"",stockTicker: "", price: 0 , volume: 0, buyOrSell:"",statusCode:0 }

  constructor(
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  addStocks() {
    this.restApi.addStocks(this.stockdetails).subscribe((data: {}) => {
      this.router.navigate(['/'])
    })
  }

}
